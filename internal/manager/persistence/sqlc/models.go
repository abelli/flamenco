// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0

package sqlc

import (
	"database/sql"
	"encoding/json"
	"time"
)

type Job struct {
	ID                      int64
	CreatedAt               time.Time
	UpdatedAt               sql.NullTime
	UUID                    string
	Name                    string
	JobType                 string
	Priority                int64
	Status                  string
	Activity                string
	Settings                json.RawMessage
	Metadata                json.RawMessage
	DeleteRequestedAt       sql.NullTime
	StorageShamanCheckoutID string
	WorkerTagID             sql.NullInt64
}

type JobBlock struct {
	ID        int64
	CreatedAt time.Time
	JobID     int64
	WorkerID  int64
	TaskType  string
}

type LastRendered struct {
	ID        int64
	CreatedAt time.Time
	UpdatedAt sql.NullTime
	JobID     int64
}

type SleepSchedule struct {
	ID         int64
	CreatedAt  time.Time
	UpdatedAt  sql.NullTime
	WorkerID   int64
	IsActive   bool
	DaysOfWeek string
	StartTime  string
	EndTime    string
	NextCheck  sql.NullTime
}

type Task struct {
	ID            int64
	CreatedAt     time.Time
	UpdatedAt     sql.NullTime
	UUID          string
	Name          string
	Type          string
	JobID         int64
	Priority      int64
	Status        string
	WorkerID      sql.NullInt64
	LastTouchedAt sql.NullTime
	Commands      json.RawMessage
	Activity      string
}

type TaskDependency struct {
	TaskID       int64
	DependencyID int64
}

type TaskFailure struct {
	CreatedAt time.Time
	TaskID    int64
	WorkerID  int64
}

type Worker struct {
	ID                 int64
	CreatedAt          time.Time
	UpdatedAt          sql.NullTime
	UUID               string
	Secret             string
	Name               string
	Address            string
	Platform           string
	Software           string
	Status             string
	LastSeenAt         sql.NullTime
	StatusRequested    string
	LazyStatusRequest  bool
	SupportedTaskTypes string
	DeletedAt          sql.NullTime
	CanRestart         bool
}

type WorkerTag struct {
	ID          int64
	CreatedAt   time.Time
	UpdatedAt   sql.NullTime
	UUID        string
	Name        string
	Description string
}

type WorkerTagMembership struct {
	WorkerTagID int64
	WorkerID    int64
}
