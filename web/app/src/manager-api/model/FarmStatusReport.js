/**
 * Flamenco manager
 * Render Farm manager API
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import FarmStatus from './FarmStatus';

/**
 * The FarmStatusReport model module.
 * @module model/FarmStatusReport
 * @version 0.0.0
 */
class FarmStatusReport {
    /**
     * Constructs a new <code>FarmStatusReport</code>.
     * @alias module:model/FarmStatusReport
     * @param status {module:model/FarmStatus} 
     */
    constructor(status) { 
        
        FarmStatusReport.initialize(this, status);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, status) { 
        obj['status'] = status;
    }

    /**
     * Constructs a <code>FarmStatusReport</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/FarmStatusReport} obj Optional instance to populate.
     * @return {module:model/FarmStatusReport} The populated <code>FarmStatusReport</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new FarmStatusReport();

            if (data.hasOwnProperty('status')) {
                obj['status'] = FarmStatus.constructFromObject(data['status']);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/FarmStatus} status
 */
FarmStatusReport.prototype['status'] = undefined;






export default FarmStatusReport;

